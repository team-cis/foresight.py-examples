# Examples foresight.py 
Some examples using the foresight.py library.
## Prerequisites
-  [Docker Desktop](https://docs.docker.com/desktop/)
-  [Docker Compose](https://docs.docker.com/compose/install/) (Comes with docker on almost all platforms)
## Run
Create a `.env` file in project root with the following content:
```
FS_USERNAME=foresight-username
FS_PASSWORD=your-password
```
You can also manually set the credentials in the notebook (and potentially leak them):
```
foresight.setUserCredentials('foresight-username', 'your-password')
```
- Run `docker-compose up` from project root
- Open [http://localhost:8888](http://localhost:8888) in a browser
- Enter password `foresight`
## People
Author: Sebastian Alberternst <sebastian.alberternst@dfki.de>
## Copyright
2021 DFKI-ASR / Team CIS
## License
MIT